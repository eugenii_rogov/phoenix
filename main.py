from dotenv import load_dotenv
from src.data_providers.data_holder import DataHolder
from src.models.chart_creator import ChartCreator
import datetime


if __name__ == '__main__':
    load_dotenv()
    data_holder = DataHolder()
    # chart_creator = ChartCreator(data_holder.books_statistics)
    data_holder.add_author("Zhenya", "Rogov", datetime.date(1997, 12, 23), None)
    data_holder.add_book("Main", "Fiction", "smthg", 1000, 2012, 1)
    print(data_holder._db_manager.get_author_by_id(1).lived_years)
    # chart_creator.create_charts()