import pytest
import psycopg2
from src.data_providers.data_holder import DataHolder
from src.tools.errors import EmptyArgValueError

from collections import namedtuple

TestCase = namedtuple("TestCase", ["book_title", "book_janre", "book_desc", 
"book_pages", "book_publish_year", "author_id", "is_ok"])
cases = [
    TestCase("test", "test", "test", "100", "100", 1, True),
    TestCase("test", "test1", "test1", 1024, 1024, 1, True),
    TestCase("", "", "", "", "", 1, True),
]


@pytest.mark.parametrize("test_case", cases)
def test_add_book(test_case: TestCase):
    dh = DataHolder()
    if test_case.is_ok:
        dh.add_book(test_case.book_title, test_case.book_janre,
        test_case.book_desc, test_case.book_pages, test_case.book_publish_year, author=test_case.author_id)
    else:
        with pytest.raises((psycopg2.Error, EmptyArgValueError)):
            dh.add_book(test_case.book_title, test_case.book_janre,
            test_case.book_desc, test_case.book_pages, test_case.book_publish_year, author=test_case.author_id)
    dh.del_book(test_case.book_title)


