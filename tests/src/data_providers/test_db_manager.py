import pandas as pd
from src.data_providers.data_holder import DataHolder

def test_get_data_as_df():
    dh = DataHolder()
    assert isinstance(dh._db_manager.get_data_as_df(), pd.DataFrame)

def test_get_data():
    dh = DataHolder()
    assert isinstance(dh._db_manager.get_data(), list)