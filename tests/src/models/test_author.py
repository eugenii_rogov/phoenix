# from collections import namedtuple
# import datetime

# import pytest
# from src.models.author import Author

# a1 = Author("test", "test", datetime.date(1991, 2, 10))
# a2 = Author("test2", "test2", datetime.date(1812, 4, 25), datetime.date(1902, 5, 5))


# TestCase = namedtuple("TestCase", ["author", "years"])
# test_cases = [
#     TestCase(a1, datetime.today().date() - a1._birth_date),
#     TestCase(a2, 90)
# ]

# @pytest.mark.parametrize("author, years", test_cases)
# def test_lived_years_prop(author: Author, years):
#     assert author.lived_years == years
