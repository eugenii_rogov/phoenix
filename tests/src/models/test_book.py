from collections import namedtuple

import pytest

from src.models.book import Book
from src.tools.errors import EmptyArgValueError


TestCase = namedtuple(
    "BookCase", ["title", "janre", "description", "pages", "publish_year", "author", "will_exception"]
)

cases = [
    TestCase("test", "test", "test", 10, 100, None, False),
    TestCase("test", "test", "test", "50", 100, None, False),
    TestCase("test", "test", "test", None, 20, "Smone", True),
    TestCase("", "test", "test", 20, 100, None, True),
    TestCase("test", "", "test", 20, "100", None, True),
    TestCase("test", "test", "", 20, "100", None, True),
]


@pytest.mark.parametrize("case", cases)
def test_book_initilizing(case: TestCase):
    if case.will_exception:
        with pytest.raises((EmptyArgValueError, ValueError)):
            Book(case.title, case.janre, case.description, case.pages, case.publish_year, author=case.author)
    else:
        assert Book(case.title, case.janre, case.description, case.pages, case.publish_year, author=case.author)
