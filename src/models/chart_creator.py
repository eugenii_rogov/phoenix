from src.models.books_statistics import BooksStatistics
import matplotlib.pyplot as plt


class ChartCreator:
    def __init__(self, books_statistics: BooksStatistics) -> None:
        self._book_statistics = books_statistics

    def create_charts(self):

        plt.style.use("_mpl-gallery")

        _, ax = plt.subplots()

        ax.hist(
            self._book_statistics.publish_years,
            bins=8,
            linewidth=0.5,
            edgecolor="white",
        )

        plt.show()
