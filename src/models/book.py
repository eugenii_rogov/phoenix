from typing import Union

from src.tools.errors import EmptyArgValueError
from src.models.author import Author


class Book:
    def __init__(
        self,
        title: str,
        janre: str,
        description: str,
        pages: int,
        publish_year: int,
        author: Union[Author, None],
    ) -> None:
        """Initialize book object

        Args:
            title (str): Book's title
            janre (str): Book's janre
            description (str): Book's description
            pages (int): Pages count
            publish_year (int): Book's publish year
            author (Author): Author object. Optional 
        """
        if title:
            self._title = title
        else:
            raise EmptyArgValueError("title")

        if janre:
            self._janre = janre
        else:
            raise EmptyArgValueError("janre")

        if description:
            self._description = description
        else:
            raise EmptyArgValueError("description")

        if publish_year:
            self._publish_year = publish_year
        else:
            raise EmptyArgValueError("publish year")

        if pages:
            if not isinstance(pages, int):
                try:
                    pages = int(pages)
                except ValueError as ex:
                    raise ex("Pages must be intager value")
            self._pages = pages
        else:
            raise EmptyArgValueError("pages")

        self._author = author

    @property
    def title(self):
        return self._title

    @property
    def janre(self):
        return self._janre

    @property
    def description(self):
        return self._description

    @property
    def pages(self):
        return self._pages

    @property
    def publish_year(self):
        return self._publish_year

    def __repr__(self) -> str:
        return f"{self._title}"
