import pandas as pd
from typing import List

from src.models.book import Book


class BooksStatistics:
    def __init__(self, books: List[Book]) -> None:
        """Initialize books statistics object

        Args:
            books (List[Book]): List of book objects
        """
        self._books = books

    @property
    def publish_years(self) -> pd.Series:
        publish_years = []
        for book in self._books:
            publish_years.append(book.publish_year)
        return pd.Series(data=publish_years, name="publish_years")

    @property
    def authors_books(self) -> pd.Series:
        authors_books = {}
        for book in self._books:
            authors_books.setdefault(book.author, []).append(book.title)
        return pd.Series(data=authors_books, name="author_books")

    def update_books(self, book: Book):
        """Update books var

        Args:
            book (Book): Book object
        """
        self._books.append(book)
