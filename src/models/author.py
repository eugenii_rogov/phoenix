from datetime import datetime
from typing import Union
from functools import cached_property
from math import floor


class Author:
    def __init__(
        self,
        id: int,
        name: str,
        surname: str,
        birth_date: datetime.date,
        death_date: Union[datetime.date, None] = None,
    ) -> None:
        self._id = id
        self._name = name
        self._surname = surname
        self._birth_date = birth_date
        self._death_date = death_date
        self._books = None

    @cached_property
    def lived_years(self) -> int:
        if self._death_date:
            days = (self._death_date - self._birth_date).days
        else:
            days = (datetime.today().date() - self._birth_date).days
        return floor(days / 365)

    @cached_property
    def full_name(self) -> str:
        return f"{self._surname} {self._name}" 