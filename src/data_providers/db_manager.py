from datetime import datetime
from typing import List, Tuple, Union

import psycopg2
import pandas as pd
from src.data_providers.db_connector import DbConnector
from src.models.author import Author


class DbManager:
    def __init__(self, db_connector: DbConnector = None) -> None:
        """Initialize db manager

        Args:
            db_connector (DbConnector): Db connector
        """
        if not db_connector:
            db_connector = DbConnector()
        self._connector = db_connector

    def get_data(self) -> List[Tuple]:
        """Get data from table books

        Returns:
            List[Tuple]: Data from rows of table
        """
        with self._connector as (cursor, _):
            cursor.execute(f"SELECT * FROM {self._connector.books_table_name}")
            data = cursor.fetchall()
        return data

    def get_author_by_id(self, id: int) -> Author:
        if id:
            query = f"SELECT * FROM {self._connector.authors_table_name} WHERE id = %s" % id
            with self._connector as (cursor, _):
                cursor.execute(query)
                data = cursor.fetchall()
            auth_id, name, surname, birth_date, death_date = data[0]
            return Author(auth_id, name, surname, birth_date, death_date) 

    def get_data_as_df(self) -> pd.DataFrame:
        query = f"SELECT * FROM {self._connector.books_table_name}"
        with self._connector as (_, conn):
            data = pd.read_sql_query(query, conn)
        return data

    def add_book(
        self, title: str, janre: str, description: str, pages: int, publish_year: int, author: Author,
    ) -> bool:
        """Add data to database and return status of execution

        Args:
            title (str): Book's title
            janre (str): Book's janre
            description (str): Book's description
            pages (int): Book's pages count
            publish_year (int): Book's publish year
        Returns:
            bool: Status of execution
        """
        added = False
        with self._connector as (cursor, _):
            try:
                cursor.execute(
                    f"INSERT INTO {self._connector.books_table_name} (title, janre, description, pages, publish_year, author_id)\
                    VALUES (%s, %s, %s, %s, %s, %s);",
                    (title, janre, description, pages, publish_year, author),
                )
                added = True
            except psycopg2.Error:
                pass
            self._connector.commit()
        return added

    def add_author(self, name: str, surname: str, birth_date: datetime.date, death_date: Union[datetime.date, None]) -> bool:
        added = False
        with self._connector as (cursor, _):
            # try:
                cursor.execute(
                    f"INSERT INTO {self._connector.authors_table_name} (name, surname, birth_date, death_date)\
                    VALUES (%s, %s, %s, %s);", (name, surname, birth_date, death_date)
                )
                added = True
            # except psycopg2.Error:
                # pass
                self._connector.commit()
        return added


    def del_book(self, title: str) -> None:
        """Delete row from database

        Args:
            title (str): Book's title
        """
        with self._connector as (cursor, _):
            try:
                cursor.execute(
                    f"DELETE FROM {self._connector.books_table_name} WHERE title = '{title}';",
                )
            except psycopg2.Error:
                raise psycopg2.Error()
            self._connector.commit()
