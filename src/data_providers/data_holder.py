from datetime import datetime
from typing import List, Union

from src.data_providers.db_manager import DbManager
from src.models.book import Book
from src.models.books_statistics import BooksStatistics
from src.models.author import Author


class DataHolder:
    def __init__(self, db_manager: DbManager = None) -> None:
        if not db_manager:
            db_manager = DbManager()
        self._db_manager = db_manager
        books = [
            Book(
                title=title,
                janre=janre,
                description=description,
                pages=pages,
                publish_year=publish_year,
                author=self._db_manager.get_author_by_id(author_id)
            )
            for _, title, janre, description, pages, publish_year, author_id in self._db_manager.get_data()
        ]
        self._books_statistics = BooksStatistics(books)

    def add_book(
        self,
        title: str,
        janre: str,
        description: str,
        pages: Union[int, str],
        publish_year: Union[int, str],
        author: Union[Author, None]
    ) -> None:
        """Add book to db and variable

        Args:
            title (str): Book's title
            janre (str): Book's janre
            description (str): Book's description
            pages (Union[int, str]): Book's pages count
            publish_year (Union[int, str]): Book's publish year
        """
        added = self._db_manager.add_book(
            title, janre, description, pages, publish_year, author
        )
        if added:
            self._books_statistics.update_books(
                Book(title, janre, description, pages, publish_year, author)
            )
    
    def add_author(self, name: str, surname: str, birth_date: datetime.date, death_date: Union[datetime.date, None]) -> None:
        """Add author to db

        Args:
            name (str): Name
            surname (str): Surname
            birth_date (datetime.date): Birth date
            death_date (Union[datetime.date, None]): Death name
        """
        added = self._db_manager.add_author(
            name, surname, birth_date, death_date
        )

    def del_book(self, title: str) -> None:
        self._db_manager.del_book(title)

    @property
    def books_statistics(self) -> List[Book]:
        """Get list of books objects

        Returns:
            List[Book]: List of books objects
        """
        return self._books_statistics
