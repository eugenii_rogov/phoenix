import psycopg2
import os


class DbConnector:
    def __init__(
        self,
        db_name: str = None,
        db_user: str = None,
        db_password: str = None,
        db_host: str = None,
        db_port: str = None,
        db_books_table_name: str = None,
        db_authors_table_name: str = None,
    ) -> None:
        """Initialize db manager object

        Args:
            db_name (str, optional): Db name. Defaults to None.
            db_user (str, optional): Db user. Defaults to None.
            db_password (str, optional): Db password. Defaults to None.
            db_host (str, optional): Db host. Defaults to None.
            db_port (str, optional): Db port. Defaults to None.
            db_books_table_name (str, optional): Books table name. Defaults to None.
            db_authors_table_name (str, optional): Authors table name. Defaults to None.
        """
        self._db_name = os.environ.get("DB_NAME") if not db_name else db_name
        self._user = os.environ.get("DB_USER") if not db_user else db_user
        self._host = os.environ.get("DB_HOST") if not db_host else db_host
        self._port = os.environ.get("DB_PORT") if not db_port else db_port
        self._password = (
            os.environ.get("DB_PASSWORD") if not db_password else db_password
        )
        self._books_table_name = (
            os.environ.get("DB_BOOKS_TABLE_NAME") if not db_books_table_name else db_books_table_name
        )

        self._authors_table_name = (
            os.environ.get("DB_AUTHORS_TABLE_NAME") if not db_authors_table_name else db_authors_table_name
        )

    def __enter__(self):
        try:
            self._conn = psycopg2.connect(
                host=self._host,
                port=self._port,
                database=self._db_name,
                user=self._user,
                password=self._password,
            )
        except:
            raise ConnectionError()
        return self._conn.cursor(), self._conn

    def __exit__(self, type, value, traceback):
        self._conn.cursor().close()
        self._conn.close()

    def commit(self):
        self._conn.commit()

    @property
    def books_table_name(self):
        return self._books_table_name

    @property
    def authors_table_name(self):
        return self._authors_table_name