from email import message


class EmptyArgValueError(Exception):
    def __init__(self, arg_name) -> None:
        self._message = f"Empty value of {arg_name} argument"
        super().__init__(self._message)
